FROM node:latest

WORKDIR /usr/app

RUN npm install arga11y -g

CMD arga11y pa11y-ci-results.json