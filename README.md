# Arga11y


## About 

ArgA11y is a tool that generates HTML reports based on Pa11y JSON report data, with a specific focus on Argentina's accessibility regulations. It's designed as a CLI tool for integration into CI/CD pipelines, enabling the approval or denial of web application deployments. ArgA11y is available as an npm package and a Docker image in both the npm and Docker registries.


## How to use NPM package

- Install node
- Install package 
   - ```bash
     npm install arga11y -g
     ```
- Use package
  - ```bash
    arga11y <path pa11y results json>
    ```

## How to use docker image

- Install Docker
- Pull docker image
  - ```bash
    docker pull registry.hub.docker.com/martinivaldi/arga11y
    ```
- Use docker image in a container (`you should pass a pa11y result json file to analyze and probably change CMD script if you use another name for the json than the default )
  - ```bash
    docker run -t registry.hub.docker.com/martinivaldi/arga11y
    ```
