const path = require('path');

const fs = require('fs');

const EXTERNAL_LIBS_PATH = path.join(__dirname, 'external-downloaded-libraries');

const TOTAL_CRITERIAS = 38;

const getUrlSummaryId = url => `summary-${url}`;

const getUrlChartId = url => `chart-${url}`;

const getQuantityCriteriasMet = criteriasUnmet => (TOTAL_CRITERIAS - criteriasUnmet.size).toString();

const getQuantityCriteriasUnmet = criteriasUnmet => (criteriasUnmet.size).toString();

const { MAX_QUANTITY_ERRORS_ARG } = require("../surpass-max-quantity-errors");

const generateErrorCardId = (index, url) => {
  return "error-card-" + index + "-" + url;
}

/**
 * Generates an HTML report based on the accessibility evaluation results and Pa11y JSON.
 *
 * @param {object} results - Accessibility evaluation results per URL, obtained from the Pa11y JSON report.
 * @param {Map} criteriasUnmetByURLs - Map containing unmet criteria per URL.
 * @param {boolean} surpass - `true` if the maximum allowed error count was surpassed,
 *                            `false` if it was met.
 */
const generateReport = (results, criteriasUnmetByURLs, surpass) => {

  console.log("Starting generating report...")

  try {

    console.log("Loading templates...")

    let templateHTML = fs.readFileSync(path.join(__dirname, 'reporte-template.html'), 'utf8');
    let templateJS = fs.readFileSync(path.join(__dirname, 'report-template.js'), 'utf8');
    const URL_SUMMARY_HTML = fs.readFileSync(path.join(__dirname, 'url_summary_template.html'), 'utf8');
    console.log("Templates loaded succesfully")

    templateHTML = replaceHeaderContainer(surpass, templateHTML);

    templateJS = replaceGeneralChartContainer(surpass, criteriasUnmetByURLs, templateJS);

    templateHTML = replaceGeneralSummaryCaption(surpass, templateHTML);

    let generalSumarryRows = "", urlChartsHTML = "", urlChartsJS = "", urlSummary = "";

    criteriasUnmetByURLs.forEach((criteriasUnmet, url) => {
      const urlSurpass = criteriasUnmet.size > MAX_QUANTITY_ERRORS_ARG;
      const idUrlSummary = getUrlSummaryId(url);
      const idUrlChart = getUrlChartId(url);
      const urlQuantityCriteriaMet = getQuantityCriteriasMet(criteriasUnmet);
      const urlQuantityCriteriaUnmet = getQuantityCriteriasUnmet(criteriasUnmet);

      generalSumarryRows = replaceGeneralSummaryRows(generalSumarryRows, idUrlSummary, url, urlQuantityCriteriaMet, urlQuantityCriteriaUnmet, urlSurpass);

      urlChartsHTML = replaceUrlChartsHTML(urlChartsHTML, url, idUrlChart, idUrlSummary);

      urlChartsJS = replaceUrlChartsJS(urlChartsJS, urlQuantityCriteriaMet, urlQuantityCriteriaUnmet, idUrlChart);

      urlSummary = replaceUrlSummary(urlSummary, URL_SUMMARY_HTML, idUrlSummary, urlSurpass, url, criteriasUnmet, results);

      urlSummary = replaceCards(results, url, urlSummary);

    })

    templateHTML = templateHTML.replace("_URL_CHARTS", urlChartsHTML);
    templateHTML = templateHTML.replace("_GENERAL_SUMMARY_ROWS", generalSumarryRows);

    templateJS = templateJS.replace("_DRAW_CHARTS", urlChartsJS);


    templateHTML = templateHTML.replace("URL_SUMMARIES_CARDS", urlSummary);

    generateAndCopyFiles(templateHTML, templateJS);
    console.log("Report generated")
  } catch (err) {
    console.error('Error generating report:', err);
    throw new Error('An error ocurred generating the report');
  }
}

const getUnmetUrlQuantity = (criteriasUnmetByURLs) => {
  unmetQuantity = 0;
  criteriasUnmetByURLs.forEach((value) => {
    if (value.size > TOTAL_CRITERIAS) unmetQuantity++
  })
  return unmetQuantity;
}

const replaceVariablesGeneralChart = (templateJS, metQuantity, unmetQuantity) => {
  templateJS = templateJS.replace('_MET_URL_GENERAL_CHART', (metQuantity).toString())
  templateJS = templateJS.replace('_UNMET_URL_GENERAL_CHART', (unmetQuantity).toString())
  return templateJS
}

replaceGeneralChartContainer = (surpass, criteriasUnmetByURLs, templateJS) => {
  if (surpass) {
    const unmetUrlQuantity = getUnmetUrlQuantity(criteriasUnmetByURLs);
    templateJS = replaceVariablesGeneralChart(templateJS, criteriasUnmetByURLs.size - unmetUrlQuantity, unmetUrlQuantity);
  } else {
    templateJS = replaceVariablesGeneralChart(templateJS, criteriasUnmetByURLs.size, 0);
  }
  return templateJS;
}

replaceHeaderContainer = (surpass, templateHTML) => {
  ANALYSIS_EVALUATION_RESULT = surpass ?
    '<span class="result-rejected">RECHAZADO </span><img height="20px" width="20px" src="./cross.png"></img></h1><p class="result-rejected result-explanation">En una o mas de las urls analizadas se encontro que no cumplen con mas 8 criterios de accesibilidad</p>'
    : '<span class="result-approved">APROBADO </span><img height="20px" width="20px" src="./check.png"></img></h1><p class="result-approved result-explanation">En todas las urls analizadas no se encontro evidencia de que incumplan mas de 8 criterios de accesibilidad</p>';
  templateHTML = templateHTML.replace('_ANALYSIS_EVALUATION_RESULT', ANALYSIS_EVALUATION_RESULT);
  return templateHTML;
}

replaceGeneralSummaryCaption = (surpass, templateHTML) => {
  templateHTML = templateHTML.replace('_GENERAL_SUMMARY_CAPTION_COLOR', surpass ?
    'rejected-color'
    :
    'aprobed-color');
  return templateHTML;
}

const GENERAL_SUMMARY_ROW_HTML = `<tr onclick="window.location.href ='#_ID_URL_SUMMARY_TABLE'">            
<td class="summary-table-first-td">_URL_ANALYZED</td>
<td class="summary-table-second-td">_URL_QUANTIY_CRITERIA_MET</td>
<td class="summary-table-third-td">_URL_QUANTIY_CRITERIA_UNMET</td>
<td class="summary-table-last-td"><img height="20px" width="20px" src="./_SURPASS.png"></td>
</tr>`;

replaceGeneralSummaryRows = (generalSumarryRows, idUrlSummary, url, urlQuantityCriteriaMet, urlQuantityCriteriaUnmet, urlSurpass) => {

  generalSumarryRows += GENERAL_SUMMARY_ROW_HTML.replace("_ID_URL_SUMMARY_TABLE", idUrlSummary);
  generalSumarryRows = generalSumarryRows.replace("_URL_ANALYZED", url);
  generalSumarryRows = generalSumarryRows.replace("_URL_QUANTIY_CRITERIA_MET", urlQuantityCriteriaMet);
  generalSumarryRows = generalSumarryRows.replace("_URL_QUANTIY_CRITERIA_UNMET", urlQuantityCriteriaUnmet);
  generalSumarryRows = generalSumarryRows.replace("_SURPASS", urlSurpass ? "cross" : "check");
  return generalSumarryRows;
}

let ERROR_CARD_ELEMENT =
  `<li class="card" id="_CARD_ID">
    <div class="card-head">
        <span class="code break-all">_CODE<span class="tooltiptext">Estandar.Principio.Pauta.Criterio.Tecnica</span></span>
    </div>
    <div class="card-body">
        <p class="title">Selector</p>
        <p class="html-content break-all">_SELECTOR</p>
        <p class="title">Contexto</p>
        <p><code class="language-html break-all">_CONTEXT</code></p>
        <p class="title">Mensaje</p>
        <p class="break-all">_MESSAGE</p>
    </div>
</li>`

const ERRORS_CARDS = `<div class="errors-container">
<h3>_URL_ANALYZED </h3>
<ol class="cards">_ERRORS_CARDS_ELEMENTS</ol>`;

replaceCards = (results, url, urlSummary) => {
  let cards = (results[url].length == 0) ? "" : ERRORS_CARDS.replace("_URL_ANALYZED", url);
  let card = "";

  card = replaceCard(results, url, card, ERROR_CARD_ELEMENT);

  cards = cards.replace("_ERRORS_CARDS_ELEMENTS", card);

  urlSummary = urlSummary.replace("_ERRORS_CARDS", cards);

  return urlSummary;
}

const replacements = {
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  '&': '&amp;',
  "'": '&#39;'
};
const replaceHTMLSpecialChars = (inputString) => {
  return inputString.replace(/[<>"']/g, (match) => replacements[match]);
};

replaceCard = (results, url, card, ERROR_CARD_ELEMENT) => {
  results[url].forEach((resultError, i) => {
    card += ERROR_CARD_ELEMENT.replace("_CARD_ID", generateErrorCardId(i, url));
    card = card.replace("_CODE", resultError.code);
    card = card.replace("_SELECTOR", replaceHTMLSpecialChars(resultError.selector));
    card = card.replace("_CONTEXT", replaceHTMLSpecialChars(resultError.context));
    card = card.replace("_MESSAGE", resultError.message);
  });
  return card;
}

const IMG_REPLACE_CRITERIA_COMPARATIONS = new Map();
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.1.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_1.1_1_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.2.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_2.1_2_2");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.2.2_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_2.1_2_2");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.2.3_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_2.1_2_3");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.2.4_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_2.1_2_4");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.2.5_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_2.1_2_5");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.3.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_3.1_3_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.3.2_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_3.1_3_2");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.3.3_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_3.1_3_3");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.4.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_4.1_4_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.4.2_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_4.1_4_2");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.4.3_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_4.1_4_3");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.4.4_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_4.1_4_4");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_1.4.5_ONCLICK_IF_UNMET", "WCAG2AA.Principle1.Guideline1_4.1_4_5");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.1.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_1.2_1_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.1.2_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_1.2_1_2");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.2.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_2.2_2_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.2.2_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_2.2_2_2");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.3.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_3.2_3_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.4.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_4.2_4_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.4.2_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_4.2_4_2");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.4.3_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_4.2_4_3");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.4.4_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_4.2_4_4");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.4.5_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_4.2_4_5");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.4.6_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_4.2_4_6");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_2.4.7_ONCLICK_IF_UNMET", "WCAG2AA.Principle2.Guideline2_4.2_4_7");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_3.1.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle3.Guideline3_1.3_1_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_3.1.2_ONCLICK_IF_UNMET", "WCAG2AA.Principle3.Guideline3_1.3_1_2");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_3.2.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle3.Guideline3_2.3_2_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_3.2.2_ONCLICK_IF_UNMET", "WCAG2AA.Principle3.Guideline3_2.3_2_2");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_3.2.3_ONCLICK_IF_UNMET", "WCAG2AA.Principle3.Guideline3_2.3_2_3");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_3.2.4_ONCLICK_IF_UNMET", "WCAG2AA.Principle3.Guideline3_2.3_2_4");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_3.3.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle3.Guideline3_3.3_3_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_3.3.2_ONCLICK_IF_UNMET", "WCAG2AA.Principle3.Guideline3_3.3_3_2");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_3.3.3_ONCLICK_IF_UNMET", "WCAG2AA.Principle3.Guideline3_3.3_3_3");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_3.3.4_ONCLICK_IF_UNMET", "WCAG2AA.Principle3.Guideline3_3.3_3_4");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_4.1.1_ONCLICK_IF_UNMET", "WCAG2AA.Principle4.Guideline4_1.4_1_1");
IMG_REPLACE_CRITERIA_COMPARATIONS.set("_IMG_MET_CRITERIA_4.1.2_ONCLICK_IF_UNMET", "WCAG2AA.Principle4.Guideline4_1.4_1_2");

const getFirstUnmetIndex = (resultError, criteriaCode) => {

  let i = 0;

  while (i < resultError.length && !(resultError[i].code.startsWith(criteriaCode))) {
    i++;
  }
  return i;
}

const getImageMetOrUnmetCriteriaAndErrorCardIdIfUnmet = (criteriaCode, criteriasUnmet, resultError, url) => {
  if (Array.from(criteriasUnmet).some(criteriaUnmet => criteriaUnmet.startsWith(criteriaCode))) {

    return `<td class="summary-table-last-td un-met-criteria" onclick="window.location.href ='#` + generateErrorCardId(getFirstUnmetIndex(resultError, criteriaCode), url) + `'" ><img height="20px" width="20px" src="./cross.png"></td>`;
  }
  return `<td class="summary-table-last-td" ><img height="20px" width="20px" src="./check.png"></td>`;
}

replaceUrlSummary = (urlSummary, URL_SUMMARY_HTML, idUrlSummary, urlSurpass, url, criteriasUnmet, results) => {

  urlSummary += URL_SUMMARY_HTML.replace("_ID_URL_SUMMARY_TABLE", idUrlSummary);

  urlSummary = urlSummary.replace("_URL_SUMMARY_CAPTION_COLOR", urlSurpass ? "rejected-color" : "aprobed-color");
  urlSummary = urlSummary.replace("_URL_ANALYZED", url);

  IMG_REPLACE_CRITERIA_COMPARATIONS.forEach((criteriaCode, replaceValue) => {
    urlSummary = urlSummary.replace(replaceValue, getImageMetOrUnmetCriteriaAndErrorCardIdIfUnmet(criteriaCode, criteriasUnmet, results[url], url));
  });
  return urlSummary;
}

const DRAW_CHARTS = `drawChart(_URL_QUANTIY_CRITERIA_MET, 'Cumplidos', _URL_QUANTIY_CRITERIA_UNMET, 'No cumplidos', '_ID_URL_CHART');`

replaceUrlChartsJS = (urlChartsJS, urlQuantityCriteriaMet, urlQuantityCriteriaUnmet, idUrlChart) => {
  urlChartsJS += DRAW_CHARTS.replace("_URL_QUANTIY_CRITERIA_MET", urlQuantityCriteriaMet);
  urlChartsJS = urlChartsJS.replace("_URL_QUANTIY_CRITERIA_UNMET", urlQuantityCriteriaUnmet);
  urlChartsJS = urlChartsJS.replace("_ID_URL_CHART", idUrlChart);
  return urlChartsJS;
}

const URL_CHART_HTML =
  `<li>
<h2 class="chart-header" title ="_URL_ANALYZED" >_URL_ANALYZED</h2>
<div class="chart-container" id="_ID_URL_CHART" onclick="window.location.href ='#_ID_URL_SUMMARY_TABLE'"></div>
</li>`

replaceUrlChartsHTML = (urlChartsHTML, url, idUrlChart, idUrlSummary) => {
  urlChartsHTML += URL_CHART_HTML.replace("_URL_ANALYZED", url);
  urlChartsHTML = urlChartsHTML.replace("_URL_ANALYZED", url);
  urlChartsHTML = urlChartsHTML.replace("_ID_URL_CHART", idUrlChart);
  urlChartsHTML = urlChartsHTML.replace("_ID_URL_SUMMARY_TABLE", idUrlSummary);
  return urlChartsHTML;
}

generateAndCopyFiles = (templateHTML, templateJS) => {
  console.log("Generating and copying report files...");

  fs.mkdirSync("generated-report");

  fs.writeFileSync('generated-report/generated-report.html', templateHTML);
  fs.writeFileSync('generated-report/generated-report.js', templateJS);

  fs.copyFileSync(path.join(__dirname, 'report-template.css'), "generated-report/generated-report.css");
  fs.copyFileSync(path.join(__dirname, 'check.png'), "generated-report/check.png");
  fs.copyFileSync(path.join(__dirname, 'cross.png'), "generated-report/cross.png");
  fs.copyFileSync(path.join(EXTERNAL_LIBS_PATH, 'cdnjs.cloudflare.com_ajax_libs_prism_1.23.0_themes_prism.min.css'), "generated-report/cdnjs.cloudflare.com_ajax_libs_prism_1.23.0_themes_prism.min.css");
  fs.copyFileSync(path.join(EXTERNAL_LIBS_PATH, 'cdnjs.cloudflare.com_ajax_libs_prism_1.23.0_prism.min.js'), "generated-report/cdnjs.cloudflare.com_ajax_libs_prism_1.23.0_prism.min.js");
  fs.copyFileSync(path.join(EXTERNAL_LIBS_PATH, 'gstatic.com_charts_loader.js'), "generated-report/gstatic.com_charts_loader.js");
}

module.exports = generateReport;
