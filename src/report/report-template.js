google.charts.load('current', { 'packages': ['corechart'] });
google.charts.setOnLoadCallback(drawCharts);

function drawChartOneValue(isOk, value, legendLabelValue, elementId) {
    const data = google.visualization.arrayToDataTable([
        ['Met', 'Quantity'],
        [legendLabelValue, value]
    ]);

    const options = {
        colors: [isOk ? '#5cba5c' : '#d9534f'],
        width: 208.33,
        height: 250,
        chartArea: { 'width': '90%', 'height': '75%' },
        backgroundColor: 'white',
        pieHole: 0.5,
        pieSliceBorderColor: 'white',
        legend: {
            position: 'bottom', textStyle: {
                fontSize: 12
            }
        },
        pieSliceText: 'percentage',
        pieSliceTextStyle: { fontSize: 12, color: 'black' },
    };


    const chart = new google.visualization.PieChart(document.getElementById(elementId));

    chart.draw(data, options);
}

function drawChartTwoValues(met, legendLabelMet, unMet, legendLabelUnmet, elementId) {
    const data = google.visualization.arrayToDataTable([
        ['Met', 'Quantity'],
        [legendLabelMet, met],
        [legendLabelUnmet, unMet],
    ]);

    const options = {
        colors: ['#5cba5c', '#d9534f'],
        width: 208.33,
        height: 250,
        chartArea: { 'width': '90%', 'height': '75%' },
        backgroundColor: 'white',
        pieHole: 0.5,
        pieSliceBorderColor: 'white',
        legend: {
            position: 'bottom', textStyle: {
                fontSize: 12
            }
        },
        pieSliceText: 'percentage',
        pieSliceTextStyle: { fontSize: 12, color: 'white' },
    };

    const chart = new google.visualization.PieChart(document.getElementById(elementId));

    chart.draw(data, options);
}

function drawChart(met, legendLabelMet, unMet, legendLabelUnmet, elementId) {
    if (met != 0 && unMet != 0) {
        drawChartTwoValues(met, legendLabelMet, unMet, legendLabelUnmet, elementId);
    } else {
        const isOK = met != 0;
        if (isOK) {
            drawChartOneValue(isOK, met, legendLabelMet, elementId);
        } else {
            drawChartOneValue(isOK, unMet, legendLabelUnmet, elementId);
        }
    }
}


function drawCharts() {

    drawChart(_MET_URL_GENERAL_CHART, 'URLs aprobadas', _UNMET_URL_GENERAL_CHART, 'URLs rechazadas', 'general-chart');
    _DRAW_CHARTS


}