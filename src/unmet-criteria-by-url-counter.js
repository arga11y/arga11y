const getWCAGCriteria = (result) => result.code.substring(0, 37)

const getUnmetCriterias = (result) => new Set(
  result.map(error =>
    getWCAGCriteria(error))
)

const logCriteriasUnmet = (criteriasUnmetByURLs) => {
  console.log('Criterias Unmet By URLs:');
  if (criteriasUnmetByURLs.size === 0) {
    console.log('No unmet criteria found.');
  } else {
    criteriasUnmetByURLs.forEach((value, key) => {
      console.log(`  URL: ${key}`);
      console.log('  Criterias:');
      value.forEach(criteria => {
        console.log(`    - ${criteria}`);
      });
    });
  }
};

/**
 * Retrieves distinct unmet WCAG criteria by URL from the given results.
 * @param {Array} results - An array of results from pa11y evaluation with WCAG2AA by URL.
 * @returns {Map} - A Map where URLs are keys and arrays of distinct unmet criteria are values.
 */
const getDistinctUnmetCriteriasByUrl = (results) => {

  const evaluatedURLs = Object.keys(results)
  const criteriasUnmetByURLs = new Map()

  evaluatedURLs.forEach(url => {
    criteriasUnmetByURLs.set(url, getUnmetCriterias(results[url]))
  })

  logCriteriasUnmet(criteriasUnmetByURLs)

  return criteriasUnmetByURLs
}

module.exports = getDistinctUnmetCriteriasByUrl;


