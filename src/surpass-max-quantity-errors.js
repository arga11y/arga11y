/**
 * The default maximum quantity of distinct criteria errors allowed in Argentina.
 * @type {number}
 */
const MAX_QUANTITY_ERRORS_ARG = 8;

/**
 * Checks if the quantity of errors surpasses the maximum allowed {@link MAX_QUANTITY_ERRORS_ARG}.
 *
 * @param {Map} criteriasUnmetByURLs - A Map containing unmet criteria by URLs.
 * @returns {boolean} 
 * - `true` if the maximum quantity of errors is surpassed, 
 * - `false` if it is within limits, which means it has been accomplished.
 */
const surpassMaxQuantityErrors = (criteriasUnmetByURLs) => {
    console.log("Validating with the maximum quantity errors:", MAX_QUANTITY_ERRORS_ARG)
    const surpass = Array.from(criteriasUnmetByURLs.values()).some(criterias => criterias.size > MAX_QUANTITY_ERRORS_ARG);
    console.log("The max quantity of errors was surpassed:", surpass);

    return surpass
};

module.exports = {
    surpassMaxQuantityErrors,
    MAX_QUANTITY_ERRORS_ARG,
};

