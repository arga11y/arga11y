#!/usr/bin/env node

const getDistinctUnmetCriteriasByUrl = require('../src/unmet-criteria-by-url-counter')
const generateReport = require('../src/report/reporter');

const { surpassMaxQuantityErrors } = require("../src/surpass-max-quantity-errors");

/**
 * Main function that validates a JSON report and checks for specific criteria compliance.
 * Reads user-provided arguments and the path to the JSON report file.
 * Performs an analysis of the report to identify criteria unmet per URL and evaluates if
 * the maximum allowable error count is exceeded.
 * Generates a html report based on the analysis and the results obtained.
 * Exits with the following codes:
 * - 0 if the JSON is valid and meets the quantity criteria.
 * - 1 in case of technical errors.
 * - 2 if the JSON file doesn't meet the valid quantity criteria.
 */
const main = () => {
    try {

        const userArguments = process.argv.slice(2)
        const reportJsonPath = userArguments[0]

        console.log("To validate ", reportJsonPath)
        const report = require(reportJsonPath)

        const criteriasUnmetByURLs = getDistinctUnmetCriteriasByUrl(report.results)
        const surpass = surpassMaxQuantityErrors(criteriasUnmetByURLs)

        generateReport(report.results, criteriasUnmetByURLs, surpass)

        process.exit(surpass ? 2 : 0)
    } catch (e) {
        console.error("An error ocurred", e)
        process.exit(1)
    }
}

main()







