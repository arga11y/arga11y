const {spawnSync} = require('child_process');
const assert = require('assert')
const fs = require('fs');

describe('Application tests', () => {
    it('should return 1 because json file doesnt exist ', () => {
        const result = spawnSync('node', ['bin/application.js', 'non_existing.json']);
        
        assert.equal(result.status, 1);
    });

    it('should return 0 -> no-surpass-limit-value-url', () => {
        const result = spawnSync('node', ['bin/application.js', '../test/mocked-results/no-surpass-limit-value-url.json']);

        fs.rmdirSync("generated-report", { recursive: true });
        assert.equal(result.status, 0);
    });
    
    it('should return 0 -> no-surpass-middle-value-url', function () {
        const result = spawnSync('node', ['bin/application.js', '../test/mocked-results/no-surpass-middle-value-url.json']);
        
        fs.rmdirSync("generated-report", { recursive: true });
        assert.equal(result.status, 0);
    })
    
    it('should return 0 -> no-surpass-middle-value-urls', function () {
        const result = spawnSync('node', ['bin/application.js', '../test/mocked-results/no-surpass-middle-value-urls.json']);
        
        fs.rmdirSync("generated-report", { recursive: true });
        assert.equal(result.status, 0);
    })

    it('should return 0 -> no-surpass-zero-errors', function () {
        const result = spawnSync('node', ['bin/application.js', '../test/mocked-results/no-surpass-zero-errors-url.json']);
        
        fs.rmdirSync("generated-report", { recursive: true });
        assert.equal(result.status, 0);
    })
    
    it('should return 2 -> surpass-higher-value-url', function () {
        const result = spawnSync('node', ['bin/application.js', '../test/mocked-results/surpass-higher-value-url.json']);
        
        fs.rmdirSync("generated-report", { recursive: true });
        assert.equal(result.status, 2);
    })
    
    it('should return 2 -> surpass-higher-value-urls', function () {
        const result = spawnSync('node', ['bin/application.js', '../test/mocked-results/surpass-higher-value-urls.json']);
        
        fs.rmdirSync("generated-report", { recursive: true });
        assert.equal(result.status, 2);
    })
    
    it('should return 2 -> surpass-limit-value', function () {
        const result = spawnSync('node', ['bin/application.js', '../test/mocked-results/surpass-limit-value.json']);
        
        fs.rmdirSync("generated-report", { recursive: true });
        assert.equal(result.status, 2);
    })
});